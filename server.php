<?php

require_once(__DIR__ . '/vendor/autoload.php');
require_once(__DIR__ . '/version.php');

// basic global config
error_reporting(E_ERROR | E_PARSE);
define('DEBUG_MODE', true);

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

use Symfony\Component\Dotenv\Dotenv;
use TRD\Utility\ConsoleDebug;
use TRD\App;
use Symfony\Component\EventDispatcher\EventDispatcher;

function e($number, $msg, $file, $line, $vars)
{
    ob_start();
    debug_print_backtrace();
    $error = ob_get_clean();

    $error = "Error $number\nMsg $msg\nFile $file\nLine $line\n" . var_export($vars, true) . "\n\n" . $error;

    file_put_contents('debug.log', $error, FILE_APPEND);
    die();
}
//set_error_handler('e');

$dotenv = new Dotenv();
$dotenv->load(__DIR__.'/.env');
$dotenv->load(__DIR__.'/.env.local');

// basic requirements
if (empty($_ENV['APP_TIMEZONE'])) {
    die('Configure your APP_TIMEZONE in .env.local');
}
date_default_timezone_set($_ENV['APP_TIMEZONE']);

$container = array();

/*
CONTAINER: DB
 */
$container['db'] = \Doctrine\DBAL\DriverManager::getConnection(array(
    'url' => $_ENV['DATABASE_URL'],
), new \Doctrine\DBAL\Configuration());


/*
CONTAINER: MEMCACHE
 */
$container['memcache'] = null;
if ($_ENV['CACHE_ENABLED']) {
    $memcache = new \Memcached;
    $memcache->addServer($_ENV['CACHE_HOST'], $_ENV['CACHE_PORT']);
    $container['memcache'] = $memcache;
}

/*
CONTAINER: MODELS
 */
$container['models'] = array(
    'sites' => new \TRD\Model\Sites($container['memcache']),
    'prebots' => new \TRD\Model\Prebots(),
    'settings' => new \TRD\Model\Settings($container['memcache']),
    'skiplists' => new \TRD\Model\Skiplist(),
    'autorules' => new \TRD\Model\AutoRules()
);

if ($_ENV['AUTOTRADING_ENABLED']) {
    $container['models']['autorules'] = new \TRD\Model\AutoRules();
}

$container['modelsMemory'] = array(
    'pretips' => array()
);

/*
CONTAINER: EVENT DISPATCHER
 */
$dispatcher = new EventDispatcher();
$dispatcher->addSubscriber(new \TRD\EventSubscriber\CacheSubscriber($container['db'], $container['models']['settings'], $container['models']['autorules']));
$container['dispatcher'] = $dispatcher;

/*
CONTAINER: DATA_SOURCES
 */
$container['data_sources'] = array(
    'imdb' => new \TRD\DataProvider\IMDBDataProvider($container['db'], $container['dispatcher'], $container['models']['settings'])
);

/*
CONTAINER: LOG
 */
$generaLogger = new \TRD\Logger\GeneralLogger();
$container['log'] = $generaLogger;
$container['log']->debug('Starting server');

/*
CONTAINER: DATALOG
 */
$dataLogger = new \TRD\Logger\DataLogger();
$container['datalog'] = $dataLogger;


$clients = new \SplObjectStorage();
$app = new App($container);

$loop = React\EventLoop\Factory::create();
$socket = new React\Socket\TcpServer($_ENV['SERVER_HOST'] . ":" . $_ENV['SERVER_PORT'], $loop);
$socket->on('connection', function (React\Socket\ConnectionInterface $conn) use (&$app, $clients) {
    $clients->attach($conn);

    // greet the user
    $conn->write(json_encode([
        'command' => 'VERSION',
        'version' => TRD_VERSION
    ]));
    ConsoleDebug::debug('Client connected');

    // handle incoming messages
    $conn->on('data', function ($data) use (&$app, $conn, $clients) {
        // process the incoming message
        $app->process($clients, $data);
    });

    // if user goes
    $conn->on('end', function () use ($conn, $clients) {
        ConsoleDebug::debug('Client disconnected');
        $clients->detach($conn);
    });
});

// start the server
ConsoleDebug::debug(sprintf('TRD (%s) server started on port %d', TRD_VERSION, $_ENV['SERVER_PORT']));
$loop->run();
