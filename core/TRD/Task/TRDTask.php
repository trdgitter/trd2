<?php

namespace TRD\Task;

use Symfony\Component\Console\Command\Command;
use TRD\Model\Settings as SettingsModel;
use TRD\Model\Sites as SitesModel;
use Doctrine\DBAL\Connection;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use TRD\Race\Race;

abstract class TRDTask extends Command
{
    protected $sitesModel = null;
    protected $settingsModel = null;
    protected $db = null;
  
    public function __construct(Connection $db, SettingsModel $settingsModel, SitesModel $sitesModel, EventDispatcherInterface $dispatcher, Race $race)
    {
        parent::__construct();
        
        $this->db = $db;
        $this->settingsModel = $settingsModel;
        $this->sitesModel = $sitesModel;
        $this->dispatcher = $dispatcher;
        $this->race = $race;
    }
}
