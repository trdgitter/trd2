<?php

namespace TRD\Task;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use TRD\Utility\CBFTP;

class CBFTPTask extends TRDTask
{
    protected function configure()
    {
        $this->setName('trd:cbftp')
            ->setDescription('CBFTP stuff')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        foreach ($this->sitesModel->getSitesArray() as $target) {
            $bncs = $this->sitesModel->findAllBNCs($target);

            $cb = new CBFTP($this->settingsModel->get('cbftp_host'), $this->settingsModel->get('cbftp_api_port'), $this->settingsModel->get('cbftp_password'));
                
            $response = $cb->rawCapture("site stat", $bncs);
          
            $updated = false;
            if ($response) {
                if (sizeof($response['successes']) > 0) {
                    foreach ($response['successes'] as $row) {
                        $site = $row['name'];
                        $res = $row['result'];
                        $credits = CBFTP::parseStatToCredits($row['result']);
                
                        $this->sitesModel->setCredits($target, $site, $credits);
                        $updated = true;
                    }
                }
            }
            if ($updated) {
                $output->writeln("Updated <info>$target</info>");
            }
        }
        return 0;
    }
}
