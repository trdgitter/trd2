<?php

namespace TRD\Task;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Helper\Table;
use TRD\Utility\ReleaseName;
use TRD\Task\TRDTask;

class FindTag extends TRDTask
{
    const UTF8_TICK = "\u{2713}";
    const UTF8_CROSS = "\u{2717}";
  
    protected function configure()
    {
        $this->setName('trd:find_tag')
            ->setDescription('Find a tag based off site + section + rlsname')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $siteModel = $this->sitesModel;
        
        $helper = $this->getHelper('question');

        $site = $helper->ask($input, $output, new Question('Please enter the site name: '));
        if (empty($site)) {
            return $output->writeln("<error>Please enter a site</error>");
        }

        $section = $helper->ask($input, $output, new Question('Please enter the section (as outputted on IRC): '));
        if (empty($section)) {
            return $output->writeln("<error>Please enter a section</error>");
        }

        $rlsname = $helper->ask($input, $output, new Question('Please enter the rlsname: '));
        if (empty($rlsname)) {
            return $output->writeln("<error>Please enter a rlsname</error>");
        }


        $validTags = $siteModel->findValidTags($site, $section, $rlsname);
        if (!sizeof($validTags)) {
            $output->writeln("<error>Found no tags (must have failed all triggers!)</error>");
        } else {
            $output->writeln("\nFound tags:");

            $table = new Table($output);
            $table
                        ->setHeaders(['Tag'])
                        ->setRows(array($validTags))
                    ;
            $table->render();
        }

        $tagOptions = $this->settingsModel->get('tag_options');

        $rows = array();
        foreach ($validTags as $k => $t) {
            $output->writeln(sprintf("\nTesting %s:", $t));

            $rowData = array();
            $rowData[] = $t;

            // requirements
            if (isset($tagOptions->$t) and isset($tagOptions->$t->tag_requires)) {
                $passes = ReleaseName::passesRequirements($rlsname, $tagOptions->$t->tag_requires);
                if ($passes !== true) {
                    unset($validTags[$k]);
                    $rowData[] = self::UTF8_CROSS;
                } else {
                    $rowData[] = self::UTF8_TICK;
                }
            }

            // skiplist
            if (isset($tagOptions->$t) and isset($tagOptions->$t->tag_skiplist)) {
                $passes = ReleaseName::passesRegexSkiplists($rlsname, $tagOptions->$t->tag_skiplist);
                if ($passes !== true) {
                    unset($validTags[$k]);
                    $rowData[] = self::UTF8_CROSS;
                } else {
                    $rowData[] = self::UTF8_TICK;
                }
            }

            $rows[] = $rowData;
        }

        $table = new Table($output);
        $table
                    ->setHeaders(['Tag', 'Requirements', 'Skiplist'])
                    ->setRows($rows)
                ;
        $table->render();


        $validTags = array_values($validTags);

        $output->writeln("\nRemaining possible tags:");
        $table = new Table($output);
        $table
                    ->setHeaders(['Tag'])
                    ->setRows(array($validTags))
                ;
        $table->render();
        
        return 0;
    }
}
