<?php

namespace TRD\Tests\Util;

use TRD\Utility\ConsoleDebug;
use PHPUnit\Framework\TestCase;

class ConsoleDebugTest extends TestCase
{
    public function testOutput()
    {
        ConsoleDebug::prefix('test', 'white', 'black');
        ConsoleDebug::command('test', 'white', 'black', 'tag', 'hello-world', ['a','b','c']);
        ConsoleDebug::pre('Test1', 'Group');

        // assert that your calculator added the numbers correctly!
        $this->assertEquals(0, 0);
    }
}