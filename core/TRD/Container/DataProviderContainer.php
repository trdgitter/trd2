<?php

namespace TRD\Container;

use TRD\Container\TRDContainer;

class DataProviderContainer extends TRDContainer
{
    protected $VALID_ITEMS = [
      'db', 'dispatcher'
    ];
}
