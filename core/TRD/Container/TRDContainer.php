<?php

namespace TRD\Container;

use Doctrine\DBAL\Connection;

class TRDContainer
{
    protected $items = [];
    
    protected $VALID_ITEMS = [];
    
    public function __construct(Connection $db)
    {
        $this->set('db', $db);
//        if (sizeof($items) > 0) {
//            foreach ($items as $k => $v) {
//                $this->set($k, $v);
//            }
//        }
    }
  
    public function set(string $key, object $item)
    {
        if (!in_array($key, $this->VALID_ITEMS)) {
            throw new \Exception('Invalid container key');
        }
        $this->items[$key] = $item;
    }
  
    public function get(string $key)
    {
        return isset($this->items[$key]) ? $this->items[$key]: null;
    }

    public static function create() {

    }
}
