<?php

namespace TRD\Container;

use TRD\Container\TRDContainer;

class RaceContainer extends TRDContainer
{
    protected const VALID_ITEMS = [
      'db', 'dispatcher',
      'settingsModel', 'sitesModel', 'skiplistsModel',
      'log', 'datalog',
    ];
}
