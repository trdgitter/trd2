<?php

namespace TRD\Container;

use TRD\Container\TRDContainer;

class ServerContainer extends TRDContainer
{
    protected $VALID_ITEMS = [
      'db', 'dispatcher'
    ];
}
