<?php

namespace TRD\Logger;

use Psr\Log\LoggerInterface;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class DataLogger implements LoggerInterface {
    public function __construct() {
        $this->logger = new Logger('datalog');
        $dateFormat = "Y n j, g:i a";
        $output = "[%datetime%] %message% %context%\n";
        $formatter = new \Monolog\Formatter\LineFormatter($output, $dateFormat, true);
        $debugHandler = new StreamHandler($_ENV['LOG_PATH'] . '/data.debug.log', Logger::DEBUG);
        $debugHandler->setFormatter($formatter);
        $this->logger->pushHandler($debugHandler);
    }


    public function emergency($message, array $context = array()) {
        $this->logger->emergency($message, $context);
    }


    public function alert($message, array $context = array()) {
        $this->logger->alert($message, $context);
    }


    public function critical($message, array $context = array()) {
        $this->logger->critical($message, $context);
    }


    public function error($message, array $context = array()) {
        $this->logger->error($message, $context);
    }

    public function warning($message, array $context = array()) {
        $this->logger->warning($message, $context);
    }


    public function notice($message, array $context = array()) {
        $this->logger->notice($message, $context);
    }

    public function info($message, array $context = array()) {
        $this->logger->info($message, $context);
    }

    public function debug($message, array $context = array()) {
        $this->logger->debug($message, $context);
    }

    public function log($level, $message, array $context = array()) {
        $this->logger->log($message, $context);
    }
}