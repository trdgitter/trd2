<?php

namespace TRD\Utility;


use League\CLImate\CLImate;

class ConsoleDebug
{
    public static function debug($str)
    {
        $climate = new CLImate();
        $message = ':: '.$str.PHP_EOL;
        $climate->green()->inline($message);
    }

    public static function incoming($str)
    {
        $climate = new CLImate();
        $message = '> [' . date('H:i:s') . '] ' .$str.PHP_EOL;
        $climate->blue()->inline($message);
    }

    public static function datePrefix() {
        $climate = new CLImate();
        $climate->blue()->inline('> [' . date('H:i:s') . '] ');
    }

    public static function prefix(string $prefix, string $foreground, string $background) {
        $climate = new CLImate();
        $climate->inline(sprintf('<%1$s><background_%2$s> %3$s </background_%1$s></%2$s>', $foreground, $background, $prefix));
    }

    public static function command(string $command, string $foreground, string $background, string $tag, string $rlsname, array $chain) {
        $climate = new CLImate();
        self::datePrefix();
        self::prefix($command, $foreground, $background);
        $climate->black()->backgroundLightGray()->inline(' '.$tag.' ');
        $climate->inline(' ' . $rlsname . ' ');
        $climate->yellow()->inline('['.implode(',', $chain).']' . PHP_EOL);
    }

    public static function pre(string $site, string $release) {
        $climate = new CLImate();
        self::datePrefix();
        self::prefix('PRE', 'black', 'magenta');
        $climate->black()->backgroundLightGray()->inline(' ' . $site . ' ');
        $climate->inline(' ' . $release . PHP_EOL);
    }

    public static function newAffil(string $site, string $release) {
        $climate = new CLImate();
        self::datePrefix();
        self::prefix('INFO', 'black', 'blue');
        $climate->inline(' New affil <'.ReleaseName::getGroup($release).'> added for <'.$site.'>' . PHP_EOL);
    }
}
