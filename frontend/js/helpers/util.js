export const splitArray = (string, seperator = " ") =>
    string.trim().split(seperator).filter(val => val.length);