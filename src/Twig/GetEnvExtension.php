<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class GetEnvExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return [
          new TwigFunction('getenv', array($this, 'getenv')),
      ];
    }

    public function getenv($key)
    {
        return isset($_ENV[$key]) ? $_ENV[$key] : false;
    }
}
