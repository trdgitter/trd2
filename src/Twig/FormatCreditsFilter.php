<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Symfony\Component\Translation\IdentityTranslator;
use Symfony\Component\Translation\TranslatorInterface;
use Twig\Environment;

class FormatCreditsFilter extends AbstractExtension
{
    public function getFilters()
    {
        return [
              new TwigFilter('format_individual_credits', [$this, 'format']),
          ];
    }

    private static function formatSizeUnits($megaBytes)
    {
        if ($megaBytes > 1000000) {
            return round($megaBytes / 1024 / 1024, 2) . "TB";
        } elseif ($megaBytes > 1000) {
            return round(($megaBytes / 1024), 1) . "GB";
        } else {
            return floor($megaBytes) . "MB";
        }

        return round($gigaBytes, 1);
    }

    public function format($creditsObject)
    {
        $credits = array();
        foreach ($creditsObject as $bnc => $value) {
            $credits[$bnc] = self::formatSizeUnits($value);
        }
        ksort($credits, SORT_NATURAL);
        return $credits;
    }
}
