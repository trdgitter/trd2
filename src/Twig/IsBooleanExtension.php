<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class IsBooleanExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return array(
          new TwigFunction('is_boolean', array($this, 'is_boolean')),
      );
    }

    public function is_boolean($variable)
    {
        return is_bool($variable);
    }
}
