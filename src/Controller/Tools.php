<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Helper\Controller;
use TRD\Logger\DataLogger;
use TRD\Logger\GeneralLogger;
use TRD\Model\Settings as SettingsModel;
use TRD\Model\Sites as SitesModel;
use TRD\Model\AutoRules as AutoRulesModel;
use Doctrine\DBAL\Connection;
use TRD\Utility\CBFTP;
use TRD\Race\Race;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

use Symfony\Component\Validator\Constraints as Assert;

class Tools extends AbstractController
{
    /**
     * @Route("/tools/simulator", name="tools_simulator", methods={"GET|POST"})
     */
    public function simulator(Connection $db, Request $request, SettingsModel $settingsModel, SitesModel $sitesModel, \TRD\Model\Skiplist $skiplistsModel, AutoRulesModel $autoRulesModel, GeneralLogger $generalLogger, DataLogger $dataLogger, EventDispatcherInterface $dispatcher): Response
    {
        $tags = array_keys(get_object_vars($settingsModel->get('tag_options')));
        sort($tags, SORT_NATURAL | SORT_FLAG_CASE);
        $tagChoices = array_combine($tags, $tags);

        $form = $this->createFormBuilder()
              ->add('bookmark', ChoiceType::class, array(
                  'constraints' => array(new Assert\NotBlank())
                  ,'choices' => $tagChoices
              ))
              ->add('rlsname', TextType::class, array(
                  'constraints' => new Assert\NotBlank(),
                  'attr' => array('placeholder' => 'e.g. Something.S01E03.BDRIP.x264-Group')
              ))
              ->add('use_cache', ChoiceType::class, array(
                'choices' => array(
                  'Yes' => true,
                  'No' => false
                ),
                'choice_attr' => function ($val, $key, $index) {
                    return ['class' => 'form-check-input'];
                },
                // 'choice_label' => function ($value) {
                //     return $value;
                // },
                'expanded' => true,
                'label' => 'Use cache (if possible)?',
                'data' => 1
              ));

        $form = $form->getForm();

        $form->handleRequest($request);

        $results = array();
        $data = null;
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
        } else {
            if (!empty($request->get('rlsname'))) {
                $data = array(
                'rlsname' => $request->get('rlsname')
                ,'bookmark' => $request->get('bookmark')
                ,'use_cache' => ($request->get('use_cache') == 0 ? false : true)
              );
            }
        }

        $result = $raceData = $raceDataClean = $raceImmutableData = $autoResponse = null;
        if ($data !== null) {
            $siteList = array();
            foreach ($sitesModel->getData() as $siteName => $info) {
                $siteList[] = $siteName;
            }

            $race = new Race($db, $sitesModel, $settingsModel, $skiplistsModel, $generalLogger, $dataLogger, $dispatcher, (bool)$data['use_cache']);
            $race->addSites($siteList);
            $result = $race->race($data['bookmark'], $data['rlsname']);
            sort($result->invalidSites);

            $raceData = $result->data->all();
            $raceDataClean = $result->data->getCleanData();
            $raceImmutableData = $result->data->getImmutableData();

            if ($_ENV['AUTOTRADING_ENABLED']) {
                $parser = new \TRD\Parser\Rules();

                $autoResponse = $autoRulesModel->evaluate($result->chain, $result->data);
                if ($autoResponse !== false) {

                            //$command = new \TRD\Processor\ProcessorResponseCommand('APPROVED');
                            //$db->executeQuery("UPDATE race SET started = 1 WHERE rlsname = ? AND bookmark = ?", array($rlsname, $tag));
                }
            }
        }

        return $this->render('tools/simulator.twig', [
              'form' => $form->createView()
              ,'results' => $result
              ,'data' => $raceData
              ,'cleanData' => $raceDataClean
              ,'immutableData' => $raceImmutableData
                ,'autoResponse' => $autoResponse
          ]);
    }
    
    /**
     * @Route("/tools/tag_overview", name="tools_tag_overview", methods={"GET"})
     */
    public function tagOverview(SettingsModel $settingsModel, SitesModel $sitesModel): Response
    {
        $tags = [];
        foreach ($settingsModel->get('tags') as $tag) {
            $tags[$tag] = [];
        }
        ksort($tags);

        foreach ($sitesModel->getData() as $siteName => $siteInfo) {
            foreach ($siteInfo->sections as $sectionInfo) {
                foreach ($sectionInfo->tags as $tag) {
                    $tags[$tag->tag][] = (!empty($sectionInfo->bnc) ? $sectionInfo->bnc : $siteName) . ' - ' . $sectionInfo->name;
                }
            }
        }

        foreach ($tags as $k => $v) {
            sort($v);
            $tags[$k] = $v;
        }

        return $this->render('tools/tag_overview.twig', array(
            'tags' => $tags
        ));
    }
    
    /**
     * @Route("/tools/data_immutable", name="tools_data_immutable", methods={"GET"})
     */
    public function dataImmutable(Connection $db, Request $request): Response
    {
        $rows = $db->fetchAll("
              SELECT * FROM data_cache WHERE data_immutable IS NOT NULL 
              ORDER BY k ASC
            ");
            
        foreach ($rows as $k => $row) {
            $immutable = unserialize($row['data_immutable']);
            $original = unserialize($row['data']);
                
            $rows[$k]['fixed'] = [];
            foreach ($immutable as $field => $value) {
                $rows[$k]['fixed'][] = [
                      'field' => $field,
                      'from' => $original[$field],
                      'to' => $value
                  ];
            }
        }

        return $this->render('tools/data_immutable.twig', array(
              'rows' => $rows,
              'msg' => $request->get('msg')
          ));
    }
    
    /**
     * @Route("/tools/data_immutable_reset", name="tools_data_immutable_reset", methods={"GET"})
     */
    public function dataImmutableReset(Request $request, Connection $db): Response
    {
        $k = $request->get('k');
          
        if (empty($k)) {
            return $this->redirectToRoute('tools_data_immutable');
        }
          
        $db->update('data_cache', [
          'data_immutable' => null,
        ], ['k' => $request->get('k')]);

        return $this->redirectToRoute('tools.data_immutable', [
          'msg' => sprintf('Immutable data reset for key: %s', $k)
        ]);
    }
    
    /**
     * @Route("/tools/cbftp/raw", name="tools_cbftp_raw", methods={"GET|POST"})
     */
    public function cbftpRaw(Request $request, SettingsModel $settingsModel): Response
    {
        $host = $settingsModel->get('cbftp_host');
        $port = $settingsModel->get('cbftp_api_port');
        $password = $settingsModel->get('cbftp_password');
        
        $missingSettings = false;
        if (empty($host) or empty($port) or empty($password)) {
            $missingSettings = true;
        }
        
        $formView = $results = null;
        if (!$missingSettings) {
            $form = $this->createFormBuilder(['path' => '/'])
            ->add('siteList', TextType::class, array(
                'attr' => array('placeholder' => 'e.g. A,B,C')
                ,'required' => false
            ))
            ->add('command', TextType::class, array(
                'constraints' => new Assert\NotBlank(),
                'attr' => array('placeholder' => 'e.g. site stat')
            ))
            ->add('path', TextType::class, array(
                'constraints' => new Assert\NotBlank(),
            ));

            $form = $form->getForm();
            $formView = $form->createView();

            $form->handleRequest($request);

            $results = null;
            $data = null;
            if ($form->isSubmitted() && $form->isValid()) {
                $data = $form->getData();
              
                $sites = [];
                if (!empty($data['siteList'])) {
                    $sites = explode(',', $data['siteList']);
                }
            
                $cb = new CBFTP($host, $port, $password);
                $results = $cb->rawCapture($data['command'], $sites, $data['path']);
            }
        }
        
        return $this->render('tools/cbftp_raw.twig', array(
          'missingSettings' => $missingSettings,
          'results' => $results,
          'form' => $formView
        ));
    }
    
    /**
     * @Route("/tools/cbftp/missing_sections", name="tools_cbftp_missing_sections", methods={"GET|POST"})
     */
    public function cbftpMissingSections(Request $request, SettingsModel $settingsModel, SitesModel $sitesModel): Response
    {
        $host = $settingsModel->get('cbftp_host');
        $port = $settingsModel->get('cbftp_api_port');
        $password = $settingsModel->get('cbftp_password');
            
        $missingSettings = false;
        if (empty($host) or empty($port) or empty($password)) {
            $missingSettings = true;
        }
            
        $formView = $results = null;
        $ran = false;
        $standardResults = $ringResults = [];
            
        if (!$missingSettings) {
            $form = $this->createFormBuilder([]);

            $form = $form->getForm();
            $formView = $form->createView();

            $form->handleRequest($request);

            $results = null;
            $data = null;
                
            if ($form->isSubmitted() && $form->isValid()) {
                $ran = true;
                $cb = new CBFTP($host, $port, $password);
                    
                $sites = $sitesModel->getSitesArray();
                foreach ($sites as $siteName) {
                    $site = $sitesModel->getSite($siteName);

                    if (!empty($site) and isset($site->sections)) {
                        $isRing = $sitesModel->isRing($siteName);
                        if (!$isRing) {
                            $siteInfo = $cb->getSiteInfo($siteName);
                            if (!empty($siteInfo)) {
                                foreach ($site->sections as $sectionInfo) {
                                    foreach ($sectionInfo->tags as $tagInfo) {
                                        $tagExists = CBFTP::siteHasSection($siteInfo['sections'], $tagInfo->tag);
                                        if (!$tagExists) {
                                            $standardResults["$siteName"][] = $tagInfo->tag;
                                        }
                                    }
                                }
                            }
                        } else {
                            $ringResults[$siteName] = [];
                              
                            // have to treat rings a bit differently
                            $bncMap = [];
                            $bncTags = [];
                            foreach ($site->sections as $sectionInfo) {
                                if (!empty($sectionInfo->bnc)) {
                                    $bnc = $sectionInfo->bnc;
                                    if (!isset($bncTags[$bnc])) {
                                        $bncTags[$bnc] = [];
                                    }
                                    foreach ($sectionInfo->tags as $tagInfo) {
                                        $bncTags[$bnc][] = $tagInfo->tag;
                                    }
                                    $ringResults[$siteName][$bnc] = [];
                                }
                            }
                                
                            // let's actually do the checks now
                            foreach ($bncTags as $bnc => $tags) {
                                $siteInfo = $cb->getSiteInfo($bnc);
                                if (!empty($siteInfo)) {
                                    foreach ($tags as $tag) {
                                        $tagExists = CBFTP::siteHasSection($siteInfo['sections'], $tag);
                                        if (!$tagExists) {
                                            $ringResults["$siteName"]["$bnc"][] = $tag;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
                                  
        return $this->render('tools/cbftp_missing_sections.twig', [
              'missingSettings' => $missingSettings,
              'ran' => $ran,
              'standardResults' => $standardResults,
              'ringResults' => $ringResults,
              'form' => $formView
            ]);
    }
    
    /**
     * @Route("/tools/tag_finder", name="tools_tag_finder", methods={"GET|POST"})
     */
    public function tagFinder(SitesModel $sitesModel, Request $request): Response
    {
        $siteChoices = [];
        $sites = $sitesModel->getData();
        foreach ($sites as $siteName => $site) {
            $siteChoices[$siteName] = $siteName;
        }
        
        $form = $this->createFormBuilder([])
            ->add('site', ChoiceType::class, array(
                'constraints' => array(new Assert\NotBlank())
                ,'choices' => $siteChoices
            ))
            ->add('announce_string', TextType::class, array(
                'constraints' => new Assert\NotBlank(),
                'attr' => array('placeholder' => 'e.g. New dir in GAMES: Rlsname')
            ));

        $form = $form->getForm();

        $form->handleRequest($request);

        $results = array();
        $data = null;
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
        }

        return $this->render('tools/tag_finder.twig', array(
            'form' => $form->createView()
        ));
    }
    
    /**
     * @Route("/tools", name="tools_list", methods={"GET"})
     */
    public function list(): Response
    {
        return $this->render('tools/index.twig');
    }
}
