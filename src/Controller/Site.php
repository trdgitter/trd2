<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Helper\Controller;
use TRD\Model\Sites as SitesModel;
use TRD\Model\Settings as SettingsModel;
use Doctrine\DBAL\Connection;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

use Symfony\Component\Validator\Constraints as Assert;

class Site extends AbstractController
{
    /**
     * @Route("/site/add", name="site_add", methods={"GET|POST"})
     */
    public function addSite(Connection $db, SitesModel $sitesModel, Request $request): Response
    {
        $form = $this->createFormBuilder(array())
          ->add('name', TextType::class, array(
            'constraints' => new Assert\NotBlank()
        ))->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $sitesModel->addSite($data['name']);

            // redirect somewhere
            return $app->redirect($app['url_generator']->generate('site.list'));
        }

        return $this->render('site/add.twig', array(
          'form' => $form->createView()
        ));
    }
    
    /**
     * @Route("/site/{name}/edit", name="site_edit", methods={"GET|POST"})
     */
    public function editSite(SettingsModel $settingsModel, SitesModel $sitesModel, string $name): Response
    {
        $site = $sitesModel->getSite($name);
        if (empty($site)) {
            throw $this->createNotFoundException('The site does not exist');
        }
        
        $sections = $site->sections;
        usort($sections, function ($a, $b) {
            return strcmp($a->name, $b->name);
        });

        $form = $this->createFormBuilder([])
              ->add('enabled', CheckboxType::class, array(
                  'label' => 'Enabled?'
              ))
              ->getForm();

        $allSites = json_decode(json_encode($sitesModel->getData()), true);
        uksort($allSites, function ($a, $b) use ($allSites) {
            $ao = $allSites["$a"];
            $bo = $allSites["$b"];

            $rdiff = $bo['enabled'] - $ao['enabled'];
            if ($rdiff) {
                return $rdiff;
            }
            return strcmp($a, $b);
        });

        return $this->render('site/edit.twig', array(
              'siteName' => $name
              ,'site' => $site
              ,'sites' => $allSites
              ,'sections' => $sections
              ,'form' => $form->createView()
              ,'siteData' => json_encode($site)
              ,'tags' => (array)$settingsModel->get('tags')
          ));
    }
  
    /**
     * @Route("/site/list", name="site_list", methods={"GET"})
     */
    public function list(Connection $db, SitesModel $sitesModel): Response
    {
        // hack to avoid dealing with the object :<
        $sites = json_decode(json_encode($sitesModel->getData()), true);

        // TODO: try and abstract this crap away
        uksort($sites, function ($a, $b) use ($sites) {
            $ao = $sites["$a"];
            $bo = $sites["$b"];

            $rdiff = $bo['enabled'] - $ao['enabled'];
            if ($rdiff) {
                return $rdiff;
            }
            return strcmp($a, $b);
        });

        $races = $db->fetchAll("
              SELECT * FROM race ORDER BY created DESC LIMIT 10
          ");

        foreach ($sites as $siteName => $site) {
            $sites[$siteName]['untaggedSections'] = 0;
            foreach ($site['sections'] as $section) {
                if (sizeof($section['tags']) === 0) {
                    $sites[$siteName]['untaggedSections']++;
                }
            }
        }

        return $this->render('site/list.twig', array(
              'sites' => $sites
              ,'races' => $races
          ));
    }
}
