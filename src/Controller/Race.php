<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Helper\Controller;
use Doctrine\DBAL\Connection;
use App\Form\ApprovalDeletedExpiredFormType;
use TRD\Model\Settings as SettingsModel;
use TRD\Model\Sites as SitesModel;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Pagerfanta\Doctrine\DBAL\QueryAdapter;
use Doctrine\DBAL\Query\QueryBuilder;
use Pagerfanta\Pagerfanta;
use Pagerfanta\View\TwitterBootstrap4View;
use TRD\Race\RaceResult;
use App\Helper\DateTime as DateTimeHelper;

class Race extends AbstractController
{
    /**
     * @Route("/race/{id}/log", name="race_log", methods={"GET"})
     */
    public function raceLog(Connection $db, Request $request, int $id): Response
    {
        $race = $db->fetchAssoc("SELECT * FROM race WHERE id = ?", array($id));
        if (empty($race)) {
            throw $this->createNotFoundException('The race does not exist');
        }
        $race['log'] = unserialize($race['log']);
        sort($race['log']->invalidSites);

        return $this->render('race/view_log.twig', array(
          'race' => $race
          ,'data' => $race['log']->data->all()
          ,'duration' => $race['log']->getDuration()
          ,'dataLookupDuration' => $race['log']->getDataLookupDuration()
      ));
    }
  
    /**
     * @Route("/race/list", name="race_list", methods={"GET"})
     */
    public function list(Connection $db, SettingsModel $settingsModel, UrlGeneratorInterface $router, Request $request): Response
    {
        $page = 1;
        if ($request->get('page')) {
            $page = $request->get('page');
        }

        $queryBuilder = new QueryBuilder($db);
        $queryBuilder->select('r.*')->from('race', 'r')->orderBy("r.created", 'DESC');

        $tag = $request->get('tag');
        if (!empty($tag)) {
            $queryBuilder->where('r.bookmark = :tag')->setParameter('tag', $request->get('tag'));
        }
        $rlsname = $request->get('rlsname');
        if (!empty($rlsname)) {
            $queryBuilder->where('r.rlsname = :rlsname')->setParameter('rlsname', $request->get('rlsname'));
        }


        $countQueryBuilderModifier = function ($queryBuilder) {
            $queryBuilder->select('COUNT(DISTINCT r.id) AS total_results')
                    ->setMaxResults(1)->orderBy("NULL");
        };

        $adapter = new QueryAdapter($queryBuilder, $countQueryBuilderModifier);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage(100);
        $pagerfanta->setCurrentPage($page);

        $races = $adapter->getSlice($pagerfanta->getCurrentPageOffsetStart(), $pagerfanta->getMaxPerPage());
        $races = $pagerfanta->getCurrentPageResults();

        // die;

        foreach ($races as $k => $row) {
            $races["$k"]['time_ago'] = DateTimeHelper::timeAgo($row['created']);
              

            $races["$k"]['data_null'] = false;
            $races["$k"]['has_cache'] = false;
            $races["$k"]['is_catastrophe'] = false;
            $races["$k"]['duration'] = -1;
            $races["$k"]['dataLookupDuration'] = -1;
              
            $log = unserialize($races["$k"]['log']);
            if ($log instanceof \TRD\Race\RaceResult) {
                if ($log->data !== null) {
                    $data = $log->data->all();
                    $races["$k"]['has_cache'] = (isset($data['tvmaze.url']) or isset($data['imdb.url']));
                } else {
                    $races["$k"]['data_null'] = false;
                }
                $races["$k"]['is_catastrophe'] = sizeof($log->catastrophes) > 0;
                $races["$k"]['duration'] = $log->getDuration();
                $races["$k"]['dataLookupDuration'] = $log->getDataLookupDuration();
            }

            $races["$k"]['chain_info'] = array();
            if (!empty($races["$k"]['chain'])) {
                $chain = explode(",", $races["$k"]['chain']);
                $complete = explode(',', $races["$k"]['chain_complete']);
                sort($chain);
                foreach ($chain as $c) {
                    $races["$k"]['chain_info'][] = array(
                      'name' => $c,
                      'complete' => in_array($c, $complete)
                    );
                }
            }
        }

        $tags = array_keys(get_object_vars($settingsModel->get('tag_options')));
        sort($tags, SORT_NATURAL | SORT_FLAG_CASE);

        // data sources map
        $datasourceMap = array();
        foreach ($settingsModel->get('tag_options') as $t => $options) {
            $datasourceMap[$t] = (bool)(isset($options->data_sources) && sizeof($options->data_sources) > 0);
        }

        $routeGenerator = function ($page) use ($request, $router) {
            return $router->generate('race_list', array('page' => $page, 'tag' => $request->get('tag')));
        };

        $view = new TwitterBootstrap4View();
        $options = array('proximity' => 3);
        $paginationHTML = $view->render($pagerfanta, $routeGenerator, $options);

        return $this->render('race/list.twig', array(
              'races' => $races
              ,'tag' => $tag
              ,'rlsname' => $rlsname
              ,'tags' => $tags
              ,'paginationHTML' => $paginationHTML
              ,'datasourceMap' => $datasourceMap
          ));
    }
}
