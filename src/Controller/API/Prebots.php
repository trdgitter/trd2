<?php

namespace App\Controller\API;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use TRD\Model\Prebots as PrebotsModel;
use App\Helper\Controller;

class Prebots extends AbstractController
{
    /**
     * @Route("/api/prebots/save", name="api_prebots_save", methods={"POST"})
     */
    public function save(Request $request, PrebotsModel $model): Response
    {
        return Controller::handleAPISave($request, $model);
    }
    
    /**
     * @Route("/api/prebots", name="api_prebots", methods={"GET"})
     */
    public function list(PrebotsModel $model): Response
    {
        return $this->json($model->getData());
    }
}
