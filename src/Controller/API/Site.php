<?php

namespace App\Controller\API;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use TRD\Model\Sites as SitesModel;
use App\Helper\Controller;

class Site extends AbstractController
{
    /**
     * @Route("/api/site/{siteName}/testString", name="api_site_testString", methods={"POST"})
     */
    public function testString(Request $request, SitesModel $model, string $siteName): Response
    {
        if ($request->headers->get('content-type') === 'application/json') {
            $site = $model->getSite($siteName);

            $postData = json_decode($request->getContent(), true);

            $testString = $postData['testString'];
            $keyBits = explode('.', $postData['key']);
            $endBit = array_pop($keyBits);

            $extraction = \TRD\Utility\IRCExtractor::extract($site->irc->strings, array($endBit), $testString);
            $section = $extraction['section'];
            $rlsname = $extraction['rlsname'];

            return $this->json([
                'matched' => $section !== null || $rlsname !== null,
                'section' => $section, 'rlsname' => $rlsname
            ]);
        }
        return $this->json(['error' => 1]);
    }
    
    /**
     * @Route("/api/site/{siteName}/save", name="api_site_save", methods={"POST"})
     */
    public function save(Request $request, SitesModel $model, string $siteName): Response
    {
        if ($request->headers->get('content-type') === 'application/json') {
            $newData = json_decode($request->getContent());
            $model->replaceSite($siteName, $newData);
            $model->save();
            return $this->json(['success' => 1]);
        }
        return $this->json(['error' => 1]);
    }
    
    /**
     * @Route("/api/site/{siteName}", name="api_site", methods={"GET"})
     */
    public function getSite(SitesModel $model, string $siteName): Response
    {
        // :(
        return $this->json(json_decode(json_encode($model->getSite($siteName)), true));
    }
}
