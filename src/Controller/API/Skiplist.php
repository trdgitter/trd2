<?php

namespace App\Controller\API;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use TRD\Model\Skiplist as SkiplistModel;
use App\Helper\Controller;

class Skiplist extends AbstractController
{
    /**
     * @Route("/api/skiplist/save", name="api_skiplist_save", methods={"POST"})
     */
    public function save(Request $request, SkiplistModel $model): Response
    {
        return Controller::handleAPISave($request, $model);
    }
    
    /**
     * @Route("/api/skiplist", name="api_skiplist", methods={"GET"})
     */
    public function list(SkiplistModel $model): Response
    {
        return $this->json($model->getData());
    }
}
