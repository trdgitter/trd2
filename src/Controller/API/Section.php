<?php

namespace App\Controller\API;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use TRD\Model\Settings as SettingsModel;

class Section extends AbstractController
{
    /**
     * @Route("/api/section", name="api_section", methods={"GET"})
     */
    public function list(SettingsModel $model): Response
    {
        $tags = array_keys(get_object_vars($model->get('tag_options')));
        sort($tags, SORT_NATURAL | SORT_FLAG_CASE);
        return $this->json($tags);
    }
}
