<?php

namespace App\Controller\API;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use TRD\Model\AutoRules as AutoRulesModel;
use App\Helper\Controller;
use League\CommonMark\GithubFlavoredMarkdownConverter;

class Doc extends AbstractController
{
    /**
     * @Route("/api/doc/parse/{path}", name="api_doc", methods={"GET"})
     */
    public function viewDoc(AutoRulesModel $model, string $path): Response
    {
        $realPath = __DIR__ . '/../../../doc/' . $path . '.md';
        if (file_exists($realPath)) {
            $converter = new GithubFlavoredMarkdownConverter([
                'html_input' => 'strip',
                'allow_unsafe_links' => false,
            ]);

            return new Response($converter->convertToHtml(file_get_contents($realPath)));
        }
        return '';
    }
}
