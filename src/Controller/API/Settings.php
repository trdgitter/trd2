<?php

namespace App\Controller\API;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use TRD\Model\Settings as SettingsModel;
use App\Helper\Controller;

class Settings extends AbstractController
{
    /**
     * @Route("/api/settings/save", name="api_settings_save", methods={"POST"})
     */
    public function save(Request $request, SettingsModel $model): Response
    {
        return Controller::handleAPISave($request, $model);
    }
    
    /**
     * @Route("/api/settings", name="api_settings", methods={"GET"})
     */
    public function list(SettingsModel $model): Response
    {
        return $this->json($model->getData());
    }
}
