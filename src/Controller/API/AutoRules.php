<?php

namespace App\Controller\API;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use TRD\Model\AutoRules as AutoRulesModel;
use App\Helper\Controller;

class AutoRules extends AbstractController
{
    /**
     * @Route("/api/autorules/save", name="api_autorules_save", methods={"POST"})
     */
    public function save(Request $request, AutoRulesModel $model): Response
    {
        return Controller::handleAPISave($request, $model);
    }
    
    /**
     * @Route("/api/autorules", name="api_autorules", methods={"GET"})
     */
    public function list(AutoRulesModel $model): Response
    {
        return $this->json($model->getData());
    }
}
