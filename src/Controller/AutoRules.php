<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Helper\Controller;

class AutoRules extends AbstractController
{
    /**
     * @Route("/autorules", name="autorules_list", methods={"GET"})
     */
    public function list(): Response
    {
        return $this->render('autorules/list.twig');
    }
}
