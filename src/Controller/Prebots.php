<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Helper\Controller;

class Prebots extends AbstractController
{
    /**
     * @Route("/prebots/list", name="prebots_list", methods={"GET"})
     */
    public function list(): Response
    {
        return $this->render('prebots/list.twig');
    }
}
