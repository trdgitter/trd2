<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Helper\Controller;
use Doctrine\DBAL\Connection;
use App\Form\ApprovalDeletedExpiredFormType;
use TRD\Model\Settings as SettingsModel;
use TRD\Model\Sites as SitesModel;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

use Symfony\Component\Validator\Constraints as Assert;

class Approved extends AbstractController
{
    /**
     * @Route("/approved/list", name="approved_list", methods={"GET"})
     */
    public function list(Connection $db, Request $request): Response
    {
        $extra = '';
        $extraFields = array();
        $tag = $request->get('tag');
        if (!empty($tag)) {
            $extra .= ' AND bookmark = ?';
            $extraFields[] = $tag;
        }

        $show = $request->get('show');
        if (empty($show)) {
            $show = 'active';
        }

        $activeCount = $db->fetchColumn("
            SELECT COUNT(*) as total
            FROM approved AS a
            WHERE (expires >= NOW() AND (a.hits < a.maxlimit OR a.maxlimit = 0))
          ");
        $expiredCount = $db->fetchColumn("
            SELECT COUNT(*) as total
            FROM approved AS a
            WHERE (expires < NOW() or (a.maxlimit > 0 and a.hits > 0 and a.hits >= a.maxlimit))
          ");

        switch ($show) {
              case 'active':
                  $extra .= ' AND (expires >= NOW() AND (a.hits < a.maxlimit OR a.maxlimit = 0))';
              break;

              case 'expired':
                  $extra .= ' AND (expires < NOW() or (a.maxlimit > 0 and a.hits >= a.maxlimit))';
              break;
          }

        $approved = $db->fetchAll("
              SELECT
                  a.*,
                  IF(a.expires < now() OR (a.maxlimit > 0 and a.hits >= a.maxlimit), true, false) as expired
              FROM approved AS a
              WHERE 1 = 1 $extra
              ORDER BY expires DESC
          ", $extraFields);
        foreach ($approved as $k => $row) {
            //$races["$k"]['time_ago'] = timeAgo($row['created']);
        }

        return $this->render('approved/list.twig', array(
              'approved' => $approved
              ,'tag' => $tag
              ,'show' => $show
              ,'msg' => $request->get('msg')
              ,'activeCount' => $activeCount
              ,'expiredCount' => $expiredCount
          ));
    }
    
    /**
     * @Route("/approved/deleteExpired", name="approved_delete_expired", methods={"GET|POST"})
     */
    public function deleteExpired(Connection $db, Request $request): Response
    {
        $form = $this->createForm(ApprovalDeletedExpiredFormType::class);
        $form->handleRequest($request);
            
        $total = $db->fetchColumn("
              SELECT 
                COUNT(*) AS total 
              FROM 
                approved AS a 
              WHERE 
                (expires < NOW() or (a.maxlimit > 0 and a.hits >= a.maxlimit))
            ");
            
            
        if ($form->isSubmitted() && $form->isValid()) {
            $db->executeQuery("DELETE FROM approved WHERE (expires < NOW() or (maxlimit > 0 and hits >= maxlimit))");
            return $this->redirectToRoute('approved_list', ['show' => 'expired', 'msg' => 'Expired approval rules deleted successfully']);
        }
            
        return $this->render('approved/delete_expired.twig', array(
          'form' => $form->createView(),
          'total' => $total
        ));
    }
    
    /**
     * @Route("/approved/{id}/delete", name="approved_delete", methods={"GET"})
     */
    public function delete(Connection $db, Request $request, int $id): Response
    {
        $db->delete('approved', array('id' => $id));
        return $this->redirectToRoute(
            'approved_list',
            [
        'show' => 'active', 'msg' => 'Approval rule deleted successfully']
        );
    }
    
    /**
     * @Route("/approved/add", name="approved_add", methods={"GET|POST"})
     * @Route("/approved/{id}/edit", name="approved_edit", methods={"GET|POST"})
     */
    public function update(Connection $db, SettingsModel $settingsModel, SitesModel $sitesModel, Request $request, int $id = 0): Response
    {
        $tagChoices = Controller::getTagChoices($settingsModel);
        $siteChoices = Controller::getSiteChoices($sitesModel);

        $data = $db->fetchAssoc("SELECT * FROM approved WHERE id = ?", array($id));
        if (empty($data)) {
            $data = array(
                  'maxlimit' => 1
                  ,'type' => 'WILDCARD'
                );
        }

        if ($request->get('rlsname')) {
            $data['pattern'] = $request->get('rlsname');
        }
        if ($request->get('bookmark')) {
            $data['bookmark'] = $request->get('bookmark');
        }
        if ($request->get('chain')) {
            $data['chain'] = $request->get('chain');
        }

        $form =  $this->createFormBuilder($data)
                ->add('bookmark', ChoiceType::class, array(
                    'constraints' => array(new Assert\NotBlank())
                    ,'choices' => $tagChoices
                ))
                ->add('pattern', TextType::class, array(
                    'constraints' => new Assert\NotBlank(),
                    'attr' => array('placeholder' => 'e.g. Something.S01E*.BDRIP.x264-Group')
                ))
                ->add('type', ChoiceType::class, array(
                    'choices' => ['Wildcard' => 'WILDCARD', 'Regex' => 'REGEX'],
                    'expanded' => true,
                    'constraints' => new Assert\Choice(['WILDCARD', 'REGEX']),
                ))
                ->add('chain', TextareaType::class, array(
                    'attr' => array(
                        'placeholder' => 'Although this is multi-line, don\'t add newlines. Seperate BNC names with commas'
                    )
                ))
                ->add('maxlimit', TextType::class, array(
                    'constraints' => new Assert\Type(array('type' => 'numeric'))
                    ,'attr' => array(
                        'data-help' => 'Set to 0 to allow unlimited'
                    )
                ));

        if ($id > 0) {
            $form->add('hits', TextType::class, array(
                    'attr' => array('disabled' => 'disabled')
                ));
        }

        $form->add('expires', DateTimeType::class, array(
                    'data' => (
                        $id == 0 ?
                                new \DateTime('now +1day', new \DateTimeZone($_ENV['APP_TIMEZONE'])) :
                                new \DateTime($data['expires'], new \DateTimeZone($_ENV['APP_TIMEZONE']))
                    )
                ));

        $form = $form->getForm();
        $form->handleRequest($request);
                
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            // clean up
            unset($data['id'], $data['hits'], $data['created']);
            $data['expires'] = $data['expires']->format('Y-m-d H:i:s');

            $data['chain'] = str_replace(
                array(' ', ';'),
                array('',','),
                $data['chain']
            );

            if ($id == 0) {
                $db->insert('approved', $data);
            } else {
                $db->update('approved', $data, array('id' => $id));
            }

            // redirect somewhere
            return $this->redirectToRoute('approved_list', [
                    'show' => 'active'
            ]);
        }

        return $this->render('approved/update.twig', array(
                    'form' => $form->createView()
                    ,'id' => $id
            ));
    }
}
