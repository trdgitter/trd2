<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Helper\Controller;

class Skiplist extends AbstractController
{
    /**
     * @Route("/skiplist/list", name="skiplist_list", methods={"GET"})
     */
    public function list(): Response
    {
        return $this->render('skiplist/list.twig');
    }
}
