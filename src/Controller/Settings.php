<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Helper\Controller;

class Settings extends AbstractController
{
    /**
     * @Route("/settings/edit", name="settings_edit", methods={"GET"})
     */
    public function list(): Response
    {
        return $this->render('settings/edit.twig');
    }
}
