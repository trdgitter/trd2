<?php

namespace App\Helper;

use Symfony\Component\HttpFoundation\Request;
use TRD\Model\Model;
use TRD\Model\Sites as SitesModel;
use TRD\Model\Settings as SettingsModel;
use Symfony\Component\HttpFoundation\JsonResponse;

class Controller
{
    public static function handleAPISave(Request $request, Model $model): JsonResponse
    {
        if ($request->getContentType() === 'json') {
            $newData = json_decode($request->getContent());
            $model->setData($newData);
            $model->save();
            return new JsonResponse(['success' => 1]);
        }
        return new JsonResponse(['error' => 1]);
    }
    
    public static function getTagChoices(SettingsModel $settingsModel): array
    {
        $tags = array_keys(get_object_vars($settingsModel->get('tag_options')));
        sort($tags, SORT_NATURAL | SORT_FLAG_CASE);
        return array_combine($tags, $tags);
    }
    
    public static function getSiteChoices(SitesModel $sitesModel): array
    {
        $siteChoices = array();
        $siteList = $sitesModel->getData();
        foreach ($siteList as $siteName => $noop) {
            if ($noop->enabled) {
                $siteChoices[$siteName] = $siteName;
            }
        }
        ksort($siteChoices);
        return $siteChoices;
    }
}
